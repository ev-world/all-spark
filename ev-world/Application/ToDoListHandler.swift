//
//  ToDoListHandler.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-03-26.
//

import Foundation
import SwiftUI
import Combine



class TaskStore : ObservableObject{
    @Published var tasks = [Task]()
}
