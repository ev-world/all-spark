//
//  PlacesRow.swift
//  ev-world
//
//  Created by Bilal Ahmad on 2021-03-27.
//

import SwiftUI
import GooglePlaces

struct PlaceRow: View {
    // 1
    var place: GMSPlace
    
    var body: some View {
        HStack {
            // 2
            Text(place.name ?? "")
                .foregroundColor(.black)
            Spacer()
        }
    }
}
