//
//  SelectedUIView.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-03-26.
//

import SwiftUI
import Firebase


struct SelectedUIView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var isEditing = false
    @State var isRegisterVehicle = false
    @State var uid: String = ""
    @State var displayname: String = ""
    @State var email: String = ""
    @State var vehicle: String = ""
    @State var batterytype: String = ""
    @State var password: String = ""
    @State var carname: String = ""
    @State var carcharginport: String = ""
    @State var maxrangecar: String = ""
    @State var error: String = ""
    @State var success: String = ""
    @State private var showPasswordAlert = false
    @EnvironmentObject var session: SessionStore
    
    var onDismiss: () -> ()
    
    var ref = Database.database().reference()
    
    var passwordResetAlert: Alert {
                 Alert(title: Text("Reset your password"), message: Text("Please click the link in the password reset email sent to you and login again with new password"), dismissButton: .default(Text("Dismiss")){
                    session.signOut()
                     })
                }

    
    var body: some View {
        self.getUserDetial()
        return ScrollView (showsIndicators: false){
            VStack {
            HStack(spacing: 50) {
                Text("User Profile")
                    .font(.system(size: 32, weight: .heavy))
            Button(action: {
                
                self.isRegisterVehicle = true
            }) {
                HStack(spacing: 10) {
                    Image(systemName: "plus")
                    Text("My Car")
                }
            }
            }
        
            Image("electric")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 200, height: 200)
                .clipShape(Circle())
                .clipped()
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
            if (error != "") {
                Text(error)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.red)
                    .padding()
            }else if (success != "") {
                Text(success)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.green)
                    .padding()
            }
            VStack {
                if !isRegisterVehicle {
                if isEditing {
                    
                    Text("Name").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 20)
                    TextField("Name", text: self.$displayname).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                    
                  
                    Text("Email").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    TextField("Email", text: self.$email).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                        
                    Text("Password").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                            .padding(.top, 25)
                        
                    Button(action: {

                        Auth.auth().sendPasswordReset(withEmail: self.email) { error in
                            if let error = error {
                                print(error.localizedDescription)
                            }else{
                                print("send")
                                self.showPasswordAlert.toggle()
                                self.isEditing = false
                            }
                        }
                    }) {
                        Text("Reset Password")
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(Color(.blue))
                    }
                    
                 /*   Text("Vehicle Model").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                    TextField("Vehicle Model", text: self.$vehicle).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                        
                        Text("Battery Type").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                            .padding(.top, 25)

                        
                    TextField("Vehicle", text: self.$batterytype).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                     */
                }else{
                    Text("Name").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 20)
                    
                    Text(self.displayname).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                    
                  
                    Text("Email").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                    Text(self.email).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                        
                    Text("Password").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                            .padding(.top, 25)
                        
                    Text("************").font(.system(size: 15)).bold().foregroundColor(.black)
                            .padding(.top, 2)
                    
            /*        Text("Vehicle Model").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                    Text(self.vehicle).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                        
                        Text("Battery Type").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                            .padding(.top, 25)
                        
                    Text(self.batterytype).font(.system(size: 15)).bold().foregroundColor(.black)
                            .padding(.top, 2)
 */
                }
 
            }
                
                if isRegisterVehicle {
                    
                    Text("Car Name").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 20)
                    TextField("Car Name", text: self.$carname).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                    Spacer()
                    Text("Car Charging Port").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 20)
                    TextField("Car Charging Port", text: self.$carcharginport).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                    Spacer()
                    Text("Max Range of the Car (in miles").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 20)
                    TextField("Max Range of the Car (in miles", text: self.$maxrangecar).textFieldStyle(RoundedBorderTextFieldStyle()).padding(.leading, 5).font(.system(size: 20))
                                .autocapitalization(.words)
                                .disableAutocorrection(true)
                }else{
                    Text("Car Name").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                    Text(self.carname).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                    Spacer()
                    Text("Car Charging Port").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                Text(self.carcharginport).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                    Spacer()
                    Text("Max Range of the Car (in miles)").font(.system(size: 15, weight: .heavy)).bold().foregroundColor(.black)
                        .padding(.top, 25)
                    
                Text(self.maxrangecar).font(.system(size: 15)).bold().foregroundColor(.black)
                        .padding(.top, 2)
                }
                
            
            }
            
            Spacer()
            
            if isEditing{
                Button(action: {
                    self.isEditing = false
                    self.updateUserDetail()
                }) {
                    Text("Update Account Details")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(Color(.blue))
                }
                Spacer()
                Button(action: {
                    self.isEditing = false
                    self.isRegisterVehicle = false
                }) {
                    Text("Cancel Editing")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(Color(.blue))
                }
            }else{
                if !isRegisterVehicle {
                Button(action: {
                    self.isEditing = true
                    self.isRegisterVehicle = false
                }) {
                    Text("Edit Account Details")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(Color(.blue))
                }
                }
            }
                
            Spacer()
                if isRegisterVehicle{
                    Button(action: {
                        self.isEditing = false
                        self.isRegisterVehicle = false
                        self.updateUserDetail()
                    }) {
                        Text("Update Vehicle")
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(Color(.blue))
                    }
                    Spacer()
                    Button(action: {
                        self.isEditing = false
                        self.isRegisterVehicle = false
                    }) {
                        Text("Cancel Editing")
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(Color(.blue))
                    }
                }
                Spacer()
            Button(action: session.signOut) {
                Text("Signout")
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(Color(.blue))
            }
        }
    }
        .onAppear {
            self.getUserDetial()     // 3)
        }.alert(isPresented: $showPasswordAlert, content: { self.passwordResetAlert })
    }
    
    func getUserDetial(){
        self.error = ""
        self.success = ""
        let user = Auth.auth().currentUser
        if let user = user {
            self.uid = user.uid
            self.displayname = user.displayName ?? ""
            self.email = user.email ?? ""
//          let photoURL = user.photoURL
            self.ref.child("\(user.uid)").observeSingleEvent(of: .value, with: { (snapshot) in
                if !isEditing  {
                        let value = snapshot.value as? NSDictionary
                        let batterytype = value?["batterytype"] as? String ?? ""
                        let vehicle = value?["vehicle"] as? String ?? ""
                        self.batterytype = batterytype
                        self.vehicle = vehicle
                        print("Got data \(snapshot.value!)")
                }
                if !isRegisterVehicle {
                        let value = snapshot.value as? NSDictionary
                        let carname = value?["carname"] as? String ?? ""
                        let carcharginport = value?["carcharginport"] as? String ?? ""
                        let maxrangecar = value?["maxrangecar"] as? String ?? ""
                        self.carname = carname
                        self.carcharginport = carcharginport
                        self.maxrangecar = maxrangecar
                        print("Got data \(snapshot.value!)")
                }
            }) { (error) in
                print("Error::::::",error.localizedDescription)
            }
        }
    }
    
    func updateUserDetail(){
        self.error = ""
        self.success = ""
        if self.displayname != "" {
            let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
            changeRequest?.displayName = self.displayname
            changeRequest?.commitChanges { (error) in
                if let error = error {
                    self.error = error.localizedDescription
                }else{
                    self.success = "Successfully updated."
                }
            }
        }
        if self.email != "" {
            Auth.auth().currentUser?.updateEmail(to: self.email) { (error) in
                if let error = error {
                    self.error = error.localizedDescription
                }else{
                    self.success = "Successfully updated."
                }
            }
        }
//        if self.password != "" {
//            Auth.auth().currentUser?.updatePassword(to: self.password) { (error) in
//              // ...
//                if let error = error {
//                    self.error = error.localizedDescription
//                }else{
//                    self.success = "Successfully updated."
//                }
//                self.password = ""
//            }
//        }
        var object: [String: String] = [:]
        if self.displayname != "" {
            object["fullname"] = self.displayname
        }
        if self.email != "" {
            object["email"] = self.email
        }
        if self.password != "" {
            object["password"] = self.password
        }
        if self.vehicle != "" {
            object["vehicle"] = self.vehicle
        }
        if self.batterytype != "" {
            object["batterytype"] = self.batterytype
        }
        if self.carname != "" {
            object["carname"] = self.carname
        }
        if self.carcharginport != "" {
            object["carcharginport"] = self.carcharginport
        }
        if self.maxrangecar != "" {
            object["maxrangecar"] = self.maxrangecar
        }
        
        self.ref.child(self.uid).setValue(object)
    }

    struct SelectedUIView_Previews: PreviewProvider {
        static var previews: some View {
            SelectedUIView(onDismiss: {print("start")})
        }
    }
}

