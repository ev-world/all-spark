//
//  RouteView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-03-27.
//

import SwiftUI
import GoogleMaps

struct RouteView: View {
    
    @Binding var DirectionsList: String
    
    var body: some View {
       
        VStack {
            Text("Route View").bold()
            GoogleMapsRouteView( DirectionsList: "List").edgesIgnoringSafeArea(.top).frame(height: 400)
            
            NavigationView{
            List() {
                Text("Turn Left on Trafalgar Road")
                Text("Turn Right on Highway 407")
                Text("Merge onto highway 401")
                Text("Take exit onto Winston Curchill Blvd")
                Text("Turn Right on Winston Curchill Blvd")
                Text("Turn Right into the plaza")
                Text("Arrive at the Desination: Tesla Supercharger")
            }.navigationBarTitle("Directions")
            

            }
        }
        
    }
}
/*
struct RouteView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
 
}
*/
