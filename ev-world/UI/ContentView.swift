//
//  ContentView.swift
//  ev-world
//
//  Created by Bilal Ahmad on 2021-03-24.
//

import SwiftUI

struct ContentView: View {
   
    
//    var body: some View {
//        TabView{
//
//            MapView()
//                .tabItem {
//                    Image(systemName: "map")
//                    Text("Map")
//
//
//            }
//           RouteView()
//                .tabItem {
//                    Image(systemName: "paperplane")
//                    Text("Route")
//            }
//            ProspectsUIView(filter: .list)
//                .tabItem {
//                    Image(systemName: "list.dash")
//                    Text("To-Do List")
//            }
//            SelectedUIView()
//                .tabItem {
//                    Image(
//                        systemName: "person.crop.circle")
//                    Text("Account")
//                }
//
//
//            }
//
//        }
//
//
//
//
//
//
//    }
    
    
    
    @EnvironmentObject var session: SessionStore

    func getUser() {
        session.listen()

    }
    @Binding var DirectionsList : String
    var body: some View {
        
        Group {
            if (session.session != nil && session.session?.isverified == true) {
                
                NavigationMenuView(DirectionsList: self.$DirectionsList)
            } else if (session.session != nil && session.session?.isverified == false){
                UserVerification()
            }
            else {
              
                        AuthView()
                        
                
                
            }

            }.onAppear(perform: getUser)

    }
    

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView().environmentObject(SessionStore())
//    }
//}
}
