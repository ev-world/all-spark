//
//  Task.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-05-16.
//

import Foundation

struct Task : Identifiable{
    var id = Int()
    var toDoItem = String()
    
}
