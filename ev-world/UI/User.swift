//
//  User.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-05-16.
//

import Foundation

struct User {
    var uid: String
    var email: String?
    var isverified: Bool?
    
    init(uid: String, email: String?, isverified: Bool) {
        self.uid = uid
        self.email = email
        self.isverified = isverified
    }
}
