//
//  MapView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-03-27.
//

import SwiftUI
import GoogleMaps

struct MapView: View {
    @Binding var tabSelection: Int
    private let zoom: Float = 15.0
    
    var body: some View {
       
        VStack {
            Text("Map View").bold()
            GoogleMapsView().edgesIgnoringSafeArea(.top).frame(height: 300)
            
            PlacesList()
        }
    }
}

/*
struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(tabSelection: 1)
    }
}
*/
