//
//  SelectTaskUIView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-05-01.
//

import SwiftUI

struct SelectTaskUIView: View {
    let gpm = GooglePlacesManager()
    @State var itemList: [Place] = []
    @State var goBackToPreviousView: Bool = false
    @State private var tabSelection = 10
    @State var hideNavBar: Bool = true
    @State private var placeName: String = ""
    @ObservedObject var taskStore = TaskStore()
    // var task:
    // let content = gpm.getTaskPlaces(task: $newToDo)
    
    //    @Binding var newTaskStore: String
    @State var task: String
    
   
    
    
    var body: some View {
        
        var places = gpm.callWebAPI(task: self.task) { (Place) in
            self.itemList = Place
        }
        
        NavigationView {
        VStack{
            NavigationLink(destination: ToDoListUIView(tabSelection: self.$tabSelection, placeName: self.$placeName).navigationBarBackButtonHidden(true)  .navigationBarHidden(hideNavBar), isActive: $goBackToPreviousView) { EmptyView() }
            List {
                if (itemList.count  != 0){
                    ForEach(self.itemList, id: \.id) { item in
                       
                        
                        
                        Button {
                            self.goBackToPreviousView = true
                            placeName = item.name
                            
                        } label: {
                            Text("Name: \(item.name)\n Address: \(item.address)")
                        }

                        
                        
                    }}
                        
               // Text("\(itemList[0].name)" + "Here")
              //  .font(.largeTitle)
               // .background(Color.white)
                //.foregroundColor(.black)
                
                }
        }
            //ForEach()
           // {id in
                
            
            
        }
        
        
        
    }
        
        
        
    }
    



