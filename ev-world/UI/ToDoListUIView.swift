//
//  ToDoListUIView.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-03-26.
//

import SwiftUI
import Combine

struct ToDoListUIView: View {
    
    

    @Binding var tabSelection: Int
    @Binding var placeName: String

    

    @ObservedObject var taskStore = TaskStore()

    @State var newToDo : String = ""
    @State var hideNavBar: Bool = true
    @State var data: String?
    var confirmTask: some View{
            Text("Hacking with Swift")
        
    }

    var searchBar : some View{
            HStack{
                TextField("Enter in a new task", text: self.$newToDo)
                
                
                Button(action: {}){
                    NavigationLink(destination: SelectTaskUIView(task: newToDo).navigationBarBackButtonHidden(true)  .navigationBarHidden(hideNavBar)){
                        Text("Add Task")
                    }
                }
        }
          
    }
        
    
    


    func getData(completion: @escaping (String) -> Void) {
        taskStore.tasks.append(Place(id: UUID(), name: self.placeName, address: "444 Walker Ln"))

            self.newToDo = ""
    }

    

    var body: some View {
        
        
           
        
   

        NavigationView{
            
            

            VStack {
               
                Button(action: {self.tabSelection = 2}){
                               Text("Submit ToDo List")
                           }
            
                
                searchBar.padding()

                
                List{

                    ForEach(self.taskStore.tasks) {task in

                        Text(task.name).id(task)

                    }.onMove(perform: self.move) .onDelete(perform: self.delete)

                }.navigationTitle("To Do List")

                .navigationBarItems(trailing: EditButton())
                
                

                

        }  .onAppear {
            getData() { data in
             
            }}
            .onDisappear()

    }

}

    

    func move(from source: IndexSet, to destination : Int) {

        taskStore.tasks.move(fromOffsets: source, toOffset: destination)

    }

    

    func delete(at offsets : IndexSet) {

        taskStore.tasks.remove(atOffsets: offsets)

    }
}


