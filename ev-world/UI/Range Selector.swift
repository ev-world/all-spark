//
//  Range Selector.swift
//  ev-world
//
//  Created by Mohit Mamtani on 2021-06-05.
//

/*
 
 
import SwiftUI

struct Range_Selector: View {
    @State private var sliderValue: Double = 0

    
    var body: some View {
        VStack {
                    Slider(value: $sliderValue, in: 0...20)
                    Text("Current slider value: \(sliderValue, specifier: "%.2f") kms")
                }.padding()
    }
}

struct Range_Selector_Previews: PreviewProvider {
    static var previews: some View {
        Range_Selector()
    }
}
*/


import SwiftUI
import Firebase

struct Range_Selector: View {
    @State private var sliderValue: Double = 0
    @State private var sliderMaxValue: Double = 0
    @State var maxrangecar: String = ""
    @State var error: String = ""
    @State var success: String = ""
    @EnvironmentObject var session: SessionStore
    
    var ref = Database.database().reference()
    var body: some View {
        
        VStack {
                    Slider(value: $sliderValue, in: 0...sliderMaxValue)
                    Text("Current slider value: \(sliderValue, specifier: "%.2f") in miles")
                }.padding()
        .onAppear {
            self.getUserDetial()     // 3)
        }
    }
    func getUserDetial(){
        self.error = ""
        self.success = ""
        let user = Auth.auth().currentUser
        if let user = user {
            self.ref.child("\(user.uid)").observeSingleEvent(of: .value, with: { (snapshot) in
                        let value = snapshot.value as? NSDictionary
                        let maxrangecar = value?["maxrangecar"] as? String ?? ""
                        let myFloat = (maxrangecar as NSString).doubleValue
                        self.sliderMaxValue = myFloat
                        print("Got \(snapshot.value!)")
                
            }) { (error) in
                print("Error::::::",error.localizedDescription)
            }
        }
    }
}

struct Range_Selector_Previews: PreviewProvider {
    static var previews: some View {
        Range_Selector()
    }
}
