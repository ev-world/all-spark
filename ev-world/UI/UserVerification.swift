//
//  UserVerification.swift
//  ev-world
//
//  Created by Bilal Ahmad on 2021-05-07.
//

import SwiftUI


struct UserVerification: View {
    @EnvironmentObject var session: SessionStore
    var body: some View {
        return VStack {
            Text("Verify your Email ID")
                .font(.system(size: 32, weight: .heavy))

            Text("Please click the link in the verification email sent to you")
            .font(.system(size: 14))

            Button(action: session.signOut) {
                Text("Login")
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(Color(.blue))
            }
        }
    }
        
}
