//
//  NavigationView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-03-27.
//

import SwiftUI

struct NavigationMenuView: View {
    @State private var tabSelection = 1
    @State private var placeName = ""
    @Binding var DirectionsList : String
    
    var body: some View {
        TabView(selection: $tabSelection){

            MapView(tabSelection: $tabSelection)
                .tabItem {
                    Image(systemName: "map")
                    Text("Map")


                }.tag(1)
            ToDoListUIView(tabSelection: $tabSelection, placeName: $placeName)
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("To-Do List")
                }.tag(3)
            
            RouteView(DirectionsList: self.$DirectionsList)
                .tabItem {
                    Image(systemName: "paperplane")
                    Text("Route")
                }.tag(2)
            
            SelectedUIView(onDismiss: {print("start")})
                .tabItem {
                    Image(
                        systemName: "person.crop.circle")
                    Text("Account")
                }
            
            Range_Selector()
                .tabItem {
                    Image(
                        systemName: "person.crop.circle")
                    Text("Range Selector")
                }


            }

        }
}
