//
//  GoogleMapsRouteView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-03-27.
//

import SwiftUI
import GoogleMaps
import Alamofire
import SwiftyJSON



struct GoogleMapsRouteView: UIViewRepresentable {
    
    @ObservedObject var locationManager = LocationManager()
        private let zoom: Float = 10.0
    let marker = GMSMarker()
    let desinationMarker = GMSMarker()
    @State var DirectionsList : String
    
    
    
    
    func makeUIView(context: Self.Context) -> GMSMapView {
        
        let camera = GMSCameraPosition.camera(withLatitude: locationManager.latitude, longitude: locationManager.longitude, zoom: zoom)
                let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)

                return mapView
    }
    
    func updateUIView(_ mapView: GMSMapView, context: Context) {
       
        let sourceLocation = "\(locationManager.latitude),\(locationManager.longitude)"
        let destinationLocation = "\(43.598364099999998),\(-79.7862607)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLocation)&destination=\(destinationLocation)&key=AIzaSyAS9ZybSf3L7ABUS1RyN5qab2i_b5C04vE"
        
        AF.request(url
        ).responseJSON { (response) in
            guard let data = response.data else {
                return
            }
            
            do {
                let jsonData = try  JSON(data: data)
                let routes = jsonData["routes"].arrayValue
               
                
             //   print(route?.capacity)
                
                for route in routes {
//                    print(route["legs"][0]["steps"][0]["html_instructions"])
                    
                    DirectionsList = route["legs"][0]["steps"][0]["html_instructions"].stringValue
                    print(DirectionsList)
                    let overview_polyline = route["overview_polyline"].dictionary
                    let points = overview_polyline?["points"]?.string

                    let path =  GMSPath.init(fromEncodedPath: points ?? "")
        
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = .systemGreen
                    polyline.strokeWidth = 3.0
                    polyline.map = mapView
                }
            }
            catch  let error {
                print(error.localizedDescription)
            }
            
        }
        
        
        mapView.animate(toLocation: CLLocationCoordinate2D(latitude: locationManager.latitude, longitude: locationManager.longitude))
        mapView.clear()
        marker.position = CLLocationCoordinate2D(latitude: locationManager.latitude, longitude: locationManager.longitude)
        marker.title = "Current Location"
        marker.snippet = "Oakvile, ON"
        marker.map = mapView
        
        
        marker.isFlat = true
        
        desinationMarker.position  = CLLocationCoordinate2D(latitude: 43.594000, longitude: -79.788330)
        desinationMarker.title = "Tesla Supercharger"
        desinationMarker.snippet = "Mississauga, ON"
        desinationMarker.map = mapView

        
      
        
    }
    
    
    
}




struct GoogleMapsRouteView_Previews: PreviewProvider {
    static var previews: some View {
        GoogleMapsView()
    }
}




