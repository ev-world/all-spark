//
//  AuthView.swift
//  ev-world
//
//  Created by Bilal Ahmad on 2021-03-24.
//

import SwiftUI
import Firebase

struct SignInView: View {
    @State var email: String = ""
    @State var password: String = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    
    func signIn() {
        session.signIn(email: email, password: password) { (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            } else {
                self.email = ""
                self.password = ""
            }
            
        }
    }
    
    var body: some View {
        VStack {
            Text("EV-World")
                .font(.system(size: 32, weight: .heavy))
            
            Text("Sign in to Continue")
            .font(.system(size: 10, weight: .medium))
                .foregroundColor(Color(.gray))
            
            VStack(spacing: 18) {
                TextField("Email Address", text: $email)
                    .font(.system(size: 14))
                    .padding(12)
                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color(.white), lineWidth: 1))
                
                SecureField("Password", text: $password)
                    .font(.system(size: 14))
                    .padding(12)
                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color(.white), lineWidth: 1))
                
            }
            .padding(.vertical, 64)
            
            Button(action: signIn) {
                Text("Sign In")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(.white)
                    .font(.system(size: 14, weight: .bold))
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(5)
                    .padding()
            }
            
            NavigationLink(destination: PasswordResetView()) {
                HStack {
                    Text("Reset Password ?")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(Color(.blue))
                }
            }
            
            if (error != "") {
                Text(error)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            
            
            Spacer()
            
            NavigationLink(destination: SignUpView()) {
                HStack {
                    Text("I am a new user,")
                        .font(.system(size: 14, weight: .light))
                        .foregroundColor(.primary)
                    
                    Text("Create an Account.")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(Color(.blue))
                }
            }
            
        }
        .padding(.horizontal, 32)
    }
}

struct PasswordResetView: View {
    
    @State var email: String = ""
    @State var error: String = ""
    @State var successMsg: String = ""
    @EnvironmentObject var session: SessionStore
    
    var body: some View {
        VStack {
            Text("Reset Password")
                .font(.system(size: 32, weight: .heavy))
            
            Text("Reset your password to get started")
                .font(.system(size: 10, weight: .medium))
                .foregroundColor(Color(.gray))
            
            VStack(spacing: 18) {
                TextField("Email Adress", text: $email)
                    .font(.system(size: 14))
                    .padding(12)
                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color(.white), lineWidth: 1))
            } .padding(.vertical, 64)
            
            Button(action: {
                Auth.auth().sendPasswordReset(withEmail: self.email) { error in
                if let error = error {
                    self.error = error.localizedDescription
                }else{
                    self.successMsg = "Please click the link in the password reset email sent to you"
                }
            }
            
            }) {
                Text("Reset Password")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(.white)
                    .font(.system(size: 14, weight: .bold))
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(5)
            }
            
            if (successMsg != "") {
                Text(successMsg)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.green)
                    .padding()
            }
            
            if (error != "") {
                Text(error)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            
            Spacer()
            
        }.padding(.horizontal, 32)
        
    }
}

struct SignUpView: View {
    
    @State var email: String = ""
    @State var password: String = ""
    @State var error: String = ""
    @EnvironmentObject var session: SessionStore
    
    func signUp() {
        session.signUp(email: email, password: password) { (result, error) in
            if let error = error {
                self.error = error.localizedDescription
            } else {
                self.email = ""
                self.password = ""
                
            }
        
        }
    }
    
    var body: some View {
        VStack {
            Text("Create Account")
                .font(.system(size: 32, weight: .heavy))
            
            Text("Sign up to get started")
                .font(.system(size: 10, weight: .medium))
                .foregroundColor(Color(.gray))
            
            VStack(spacing: 18) {
                TextField("Email Adress", text: $email)
                    .font(.system(size: 14))
                    .padding(12)
                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color(.white), lineWidth: 1))
                
                SecureField("Password", text: $password)
                    .font(.system(size: 14))
                    .padding(12)
                    .background(RoundedRectangle(cornerRadius: 5).strokeBorder(Color(.white), lineWidth: 1))
                
                
            } .padding(.vertical, 64)
            
            Button(action: signUp) {
                Text("Create Account")
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundColor(.white)
                    .font(.system(size: 14, weight: .bold))
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(5)
            }
            if (error != "") {
                Text(error)
                    .font(.system(size: 14, weight: .semibold))
                    .foregroundColor(.red)
                    .padding()
            }
            
            Spacer()
            
        }.padding(.horizontal, 32)
        
    }
}

struct AuthView: View {
    var body: some View {
        NavigationView {
            SignInView()
        }
    }
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView().environmentObject(SessionStore())
    }
}
