//
//  GooglePlacesManager.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-05-05.
//

import Foundation
import Alamofire
import SwiftyJSON


class GooglePlacesManager  {
    @Published var places: [Place] = []
  
    func callWebAPI(task: String, complete: @escaping ([Place]) -> ()) {
        
        let baseURL = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
        let rawTask = task
        var cleanedTask = ""
        for char in rawTask {
            if char == " " {
                cleanedTask = cleanedTask + "+"
            }
            else{
                cleanedTask = cleanedTask  + "\(char)"
            }
        }
        
        let key = "key=AIzaSyAS9ZybSf3L7ABUS1RyN5qab2i_b5C04vE"
        
        let lm = LocationManager()
        let lat = "lat=\(lm.latitude)"
        let long = "lng=\(lm.longitude)"
        //let baseURL =
        let queryString = baseURL + "&" + "query=\(cleanedTask)&" + key + "&" + lat + long
        let urlString = URL(string: queryString)
        var res = JSON()
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard let data = data, error == nil else {
                    //completionHandler(nil)
                    
                    return
                }

                do {
                    let jsonData = try  JSON(data: data)
                    //print(jsonData["results"].arrayValue)
                   // completionHandler(jsonData as? [Place])
                    res = jsonData["results"]
                   // print(res)
                 //   print(route?.capacity)
                    
                    for i in 0...res.count{
                        if self.places.count == res.count{break}
                        var place = Place()
                        place.name = "\(res[i]["name"])"
                        place.address = "\(res[i]["formatted_address"])"
                        self.places.append(place)
                        
                        print(self.places.count)
                        complete(self.places)
                       // print(self.places[i].name)
                     //   print(places[i].name + places[i].address)
                    }
                    
                    
    
                   
                    
                    
                    
                } catch let parsingError {
                    //completionHandler(nil)
                }
            }
            task.resume()
           
        }
       // print(self.places)
    }
    
    

    
    
    
}
