//
//  SessionStore.swift
//  ev-world
//
//  Created by Bilal Ahmad on 2021-03-24.
//

import SwiftUI
import Firebase
import Combine

class SessionStore: ObservableObject {
    var didChange = PassthroughSubject<SessionStore, Never>()
    @Published var session: User? {didSet {self.didChange.send(self) }}
    var handle: AuthStateDidChangeListenerHandle?
     
    func listen() {
        handle = Auth.auth().addStateDidChangeListener({(auth, user) in
            if let user = user {
                self.session = User(uid: user.uid, email: user.email,isverified: user.isEmailVerified)
            } else {
                self.session = nil
            }
        })
    }
    func signUp(email: String, password: String, handler: @escaping AuthDataResultCallback) {
        Auth.auth().createUser(withEmail: email, password: password, completion: {user, error in
            if let firebaseError = error {
                            print(firebaseError.localizedDescription)
                            return
                        }

            self.sendVerificationMail()
        })
        
    }
    
    public func sendVerificationMail() {
        if Auth.auth().currentUser != nil && !Auth.auth().currentUser!.isEmailVerified {
            Auth.auth().currentUser!.sendEmailVerification(completion: { (error) in
                if error != nil {
                    print("email not sent")
                }else{
                    print("email sent")
                }
                
                // Notify the user that the mail has sent or couldn't because of an error.
            })
        }
        else {
            print("user already verified.")
            // Either the user is not available, or the user is already verified.
        }
    }
    
    func signIn(email: String, password: String, handler: @escaping AuthDataResultCallback) {
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
            self.session = nil
        } catch {
            print("Error Signing out")
        }
    }
    func unbind() {
        if let handle = handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }
    deinit {
        unbind()
    }
}


