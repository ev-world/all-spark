//
//  NavigationView.swift
//  ev-world
//
//  Created by Bradley Jones on 2021-03-27.
//

import SwiftUI

struct NavigationMenuView: View {
    
    var body: some View {
        TabView{

            MapView()
                .tabItem {
                    Image(systemName: "map")
                    Text("Map")


            }
           RouteView()
                .tabItem {
                    Image(systemName: "paperplane")
                    Text("Route")
            }
            ProspectsUIView(filter: .list)
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("To-Do List")
            }
            SelectedUIView()
                .tabItem {
                    Image(
                        systemName: "person.crop.circle")
                    Text("Account")
                }


            }

        }
}
